<?php
/**
 * Imageine Online Auto Coupon
 * Copyright (C) 2020  Imageine Online
 *
 * This file is part of Imageineonline/AutoCoupon.
 *
 * Imageineonline/AutoCoupon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Imageineonline\Base\Block\Adminhtml;

use Magento\Backend\Block\Template;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Extensions extends Field
{

    protected $_template = 'Imageineonline_Base::modules.phtml';

    private $moduleListProcessor;

    public function __construct(
        Template\Context $context,
        \Imageineonline\Base\Model\ModuleListProcessor $moduleListProcessor,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->moduleListProcessor = $moduleListProcessor;
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->toHtml();
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        return $this->moduleListProcessor->getModuleList();
    }

}

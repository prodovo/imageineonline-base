<?php
/**
 * Imageine Online
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the imageineonline.com license that is
 * available through the world-wide-web at this URL:
 * https://www.imageineonline.co.uk/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Imageine Online
 * @package     Imageineonline_Base
 * @copyright   Copyright (c) 2019 Imageine Online (http://www.imageineonline.co.uk/)
 * @license     https://www.imageineonline.co.uk/LICENSE.txt
 */

namespace Imageineonline\Base\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class NoticeType implements ArrayInterface
{
    const TYPE_ANNOUNCEMENT = 'announcement';
    const TYPE_NEWUPDATE = 'new_update';
    const TYPE_MARKETING = 'marketing';
	
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->toArray() as $value => $label) {
            $options[] = [
                'value' => $value,
                'label' => $label
            ];
        }
        return $options;
    }

    public function toArray()
    {
        return [
            self::TYPE_ANNOUNCEMENT => __('Announcement'),
            self::TYPE_NEWUPDATE => __('New & Updates'),
            self::TYPE_MARKETING => __('Promotions')
        ];
    }
}
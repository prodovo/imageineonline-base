<?php
/**
 * Imageine Online Auto Coupon
 * Copyright (C) 2020  Imageine Online
 *
 * This file is part of Imageineonline/AutoCoupon.
 *
 * Imageineonline/AutoCoupon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Imageineonline\Base\Model;

use Magento\Framework\Module\ModuleListInterface;
use Imageineonline\Base\Helper\AbstractData;

class ModuleListProcessor
{
    /**
     * @var ModuleListInterface
     */
    private $moduleList;

    /**
     * @var \Imageineonline\Base\Helper\AbstractData
     */
    private $moduleHelper;

    /**
     * @var array
     */
    private $modules;

    public function __construct(
        ModuleListInterface $moduleList,
        AbstractData $moduleHelper
    )
    {
        $this->moduleList = $moduleList;
        $this->moduleHelper = $moduleHelper;
    }

    /**
     * @return array
     */
    public function getModuleList(): array
    {
        if ($this->modules !== null) {
            return $this->modules;
        }

        $this->modules = [
            'lastVersion' => [],
            'hasUpdate' => []
        ];

        $modules = $this->moduleList->getNames();
        sort($modules);
        foreach ($modules as $moduleName) {
            if ($moduleName === 'Imageineonline_Base' || strpos($moduleName, 'Imageineonline_') === false
            ) {
                continue;
            }

            try {
                if (!is_array($module = $this->getModuleInfo($moduleName))) {
                    continue;
                }
            } catch (\Exception $e) {
                continue;
            }

            if (empty($module['hasUpdate'])) {
                $this->modules['lastVersion'][] = $module;
            } else {
                $this->modules['hasUpdate'][] = $module;
            }
        }

        return $this->modules;
    }

    /**
     * @param $moduleCode
     * @return array|mixed|string
     */
    protected function getModuleInfo($moduleCode)
    {
        $module = $this->moduleHelper->getModuleInfo($moduleCode);

        if (!isset($module['version'], $module['description']) || !is_array($module)
        ) {
            return '';
        }


        $currentVer = $module['version'];
        $module['description'] = $this->replaceIOText($module['description']);

        $lastVer = $module['version'];
        $module['lastVersion'] = $lastVer;
        $module['hasUpdate'] = version_compare($currentVer, $lastVer, '<');
        $module['description'] = $this->replaceIOText($module['title']);
        $module['url'] = !empty($module['url']) ? $module['url'] : '';

        return $module;

    }

    /**
     * @param string $moduleName
     *
     * @return string
     */
    protected function replaceIOText($moduleName)
    {
        return str_replace(['for Magento 2', 'by Imageine Online'], '', $moduleName);
    }
}

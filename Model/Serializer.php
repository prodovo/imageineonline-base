<?php
/**
 * Imageine Online Auto Coupon
 * Copyright (C) 2020  Imageine Online
 *
 * This file is part of Imageineonline/AutoCoupon.
 *
 * Imageineonline/AutoCoupon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Imageineonline\Base\Model;

use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Unserialize\Unserialize;
use Zend\Serializer\Adapter\PhpSerialize;

/**
 * Wrapper for Serialize
 * @since 1.1.0
 */
class Serializer
{
    /**
     * @var null|SerializerInterface
     */
    private $serializer;

    /**
     * @var Unserialize
     */
    private $unserialize;

    /**
     * @var PhpSerialize
     */
    private $phpSerialize;

    public function __construct(
        ObjectManagerInterface $objectManager,
        Unserialize $unserialize,
        PhpSerialize $phpSerialize //deus ex machina
    ) {
        if (interface_exists(SerializerInterface::class)) {
            $this->serializer = $objectManager->get(SerializerInterface::class);
        }
        $this->unserialize = $unserialize;
        $this->phpSerialize = $phpSerialize;
    }

    public function serialize($value)
    {
        try {
            if ($this->serializer === null) {
                return $this->phpSerialize->serialize($value);
            }

            return $this->serializer->serialize($value);
        } catch (\Exception $e) {
            return '{}';
        }
    }

    public function unserialize($value)
    {
        if (false === $value || null === $value || '' === $value) {
            return false;
        }

        if ($this->serializer === null) {
            return $this->unserialize->unserialize($value);
        }

        try {
            return $this->serializer->unserialize($value);
        } catch (\InvalidArgumentException $exception) {
            return $this->phpSerialize->unserialize($value);
        }
    }
}

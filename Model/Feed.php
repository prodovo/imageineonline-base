<?php
/**
 * Imageine Online
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the imageineonline.com license that is
 * available through the world-wide-web at this URL:
 * https://www.imageineonline.co.uk/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Imageine Online
 * @package     Imageineonline_Base
 * @copyright   Copyright (c) 2019 Imageine Online (http://www.imageineonline.co.uk/)
 * @license     https://www.imageineonline.co.uk/LICENSE.txt
 */

namespace Imageineonline\Base\Model;

/**
 * Class Feed
 * @package Imageineonline\Base\Model
 */
class Feed extends \Magento\AdminNotification\Model\Feed
{
    /**
     * @inheritdoc
     */
    const IOBASE_FEED_URL = 'www.imageineonline.co.uk/notifications.xml';

    /**
     * @inheritdoc
     */
    public function getFeedUrl()
    {
        $httpPath = $this->_backendConfig->isSetFlag(self::XML_USE_HTTPS_PATH) ? 'https://' : 'http://';
        if ($this->_feedUrl === null) {
            $this->_feedUrl = $httpPath . self::IOBASE_FEED_URL;
        }

        return $this->_feedUrl;
    }

    /**
     * @inheritdoc
     */
    public function checkUpdate()
    {

        if (!(boolean)$this->_backendConfig->getValue('imageineonline_base/notifications/notice_enable')) {
            return $this;
        }

        return parent::checkUpdate();
    }

    /**
     * @inheritdoc
     */
    public function getFeedData()
    {
        $type = $this->_backendConfig->getValue('imageineonline_base/notifications/notice_type');
        if (!$type) {
            return false;
        }

        $feedXml = parent::getFeedData();
        if ($feedXml && $feedXml->channel && $feedXml->channel->item) {
            $typeArray = explode(',', $type);
            $noteToRemove = [];

            foreach ($feedXml->channel->item as $item) {
                if (!in_array((string)$item->type, $typeArray)) {
                    $noteToRemove[] = $item;
                }
            }
            foreach ($noteToRemove as $item) {
                unset($item[0]);
            }
        }

        return $feedXml;
    }

    /**
     * @inheritdoc
     */
    public function getLastUpdate()
    {
        return $this->_cacheManager->load('imageineonlinebase_notifications_lastcheck');
    }

    /**
     * @inheritdoc
     */
    public function setLastUpdate()
    {
        $this->_cacheManager->save(time(), 'imageineonlinebase_notifications_lastcheck');

        return $this;
    }
}
